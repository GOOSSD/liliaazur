﻿/// resources
	resource=folder>>>@this@>>>+0>>>+999>>>|resources|

/// placeholder settings
	--- default behavior: {{placeholder}}  if not resolved, the placeholder stays on the page
	--- optional content placeholders: if not resolved, the placeholder will be removed from the page
	--- local placeholders: if not resolved by local content, the placeholder will be removed from the page
	placeholders=local>>>@this@>>>+0>>>+999>>>|extratext|feedback|