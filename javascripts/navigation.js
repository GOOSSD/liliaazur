﻿// initialize navigation functions
__onready.push('navigation.init');
__onready.push('navigation.elevateZoom');

// define event handling
if (!navigation) var navigation = {};
navigation.events = {
	"click":
	{
		"navigation.slide": "navigation.navigationSlide|search.hidePopups"
		, "navigation.switchImage": "navigation.switchImage"
	}
	, "resize":
	{
		"default": "navigation.fixLayout"
	}
	, "keydown":
	{
		"Keycode37": "navigation.previous"
		,"Keycode38": "navigation.up"
		,"Keycode39": "navigation.next"
		,"Keycode40": "navigation.down"
	}
}

// load events in global event handling object
__loadEvents(navigation.events);

// functions
navigation.init = function() {
	var url = window.location.href;
	var menu = url.split('#');
	if (menu[1] == 'menu') {
		navigation.menu();
	} 
};
// add elevateZoom functionality
navigation.elevateZoom = function() {
	$('.zoomContainer').remove();
	$('#zoom').elevateZoom({
		zoomType: "inner",
		cursor: "crosshair",
		zoomWindowFadeIn: 500,
		zoomWindowFadeOut: 750
	});
}
navigation.cleanupElevateZoom = function() {
	$('.zoomContainer').remove();
}
// functions
// add switching images functionality
navigation.switchImage = function(uid, event) {
	var $target = $(event.target);
	var $zoom = $('#zoom');
	var zoomSrc = $zoom.prop('src');
	var zoomLayout = $zoom.hasClass('portrait') ? 'portrait' : 'landscape';
	var targetSrc = $target.prop('src');
	var targetLayout = $target.hasClass('portrait') ? 'portrait' : 'landscape';
	var targetSrcZoom = $target.prop('src').replace('/thumbs/', '/zoom/');
	$('#zoom').hide().removeClass('portrait').removeClass('landscape').addClass(targetLayout).prop('src', targetSrc).fadeIn(1200);
	$('#zoom').data('zoom-image', targetSrcZoom);
	$(event.target).hide().removeClass('portrait').removeClass('landscape').addClass(zoomLayout).prop('src', zoomSrc).fadeIn(1200);
	$('.zoomContainer').remove();
	navigation.elevateZoom();
}

navigation.previous = function() {
	if ($('a#previous').length > 0) {
		$('a#previous')[0].click();
	}
}
navigation.up = function() {
	if ($('a#up').length > 0) {
		$('a#up')[0].click();
	}
}
navigation.next = function() {
	if ($('a#next').length > 0) {
		$('a#next')[0].click();
	}
}
navigation.down = function() {
	if ($('a#down').length > 0) {
		$('a#down')[0].click();
	}
}
navigation.menu = function() {
		$(".navigation-slide-in-out").css({"left":"0px"});
		$('.slide-in-out-slide-out-button').find('i').removeClass('i-slide-out').addClass('i-slide-in');
		$('.slide-in-out-slide-out-button').find('i').data('action', 'navigation.slide_in');
}
navigation.navigationSlide = function(uid, event, direction) {
	if (direction == 'out') {
		$('.navigation-slide-in-out').animate({"left":"0px"});
		$('.slide-in-out-slide-out-button').find('i').removeClass('i-slide-out').addClass('i-slide-in');
		$('.slide-in-out-slide-out-button').find('i').data('action', 'navigation.slide_in');
	}
	else {
		$('.navigation-slide-in-out').animate({"left":"-100%"});
		$('.slide-in-out-slide-out-button').find('i').removeClass('i-slide-in').addClass('i-slide-out');
		$('.slide-in-out-slide-out-button').find('i').data('action', 'navigation.slide_out');
	}
}
navigation.fixLayout = function() {
	if ($(window).width() > __data.breakpointPx) {
		$('.navigation-slide-in-out').animate({"left":"-100%"});
		$('.slide-in-out-slide-out-button').find('i').removeClass('i-slide-in').addClass('i-slide-out');
		$('.buttons-slide-in-out').hide();
	}
	else {
		$('.main-navigation').css('margin-top', 0);
		$('.buttons-slide-in-out').show();
	}
}
