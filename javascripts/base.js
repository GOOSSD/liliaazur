// event handlers
var __uid = [];		// used to link data with HTML 
var __onready = [];	// used to define onload functions that should run after document ready
var __onevent = [];	// used to define onevent functions that should run after events
var __data = {};	// used to store global data

$(document).ready(function(){
	$("body").removeClass("preload");
	// do document ready handling
	__onreadyHandler();
	// attach events
	// add more if needed
	$("body").click(function(event) {__eventHandler(event)});
	$("body").dblclick(function(event) {__eventHandler(event)});
	$("body").keydown(function(event) {__eventHandler(event)});
	$("body").keyup(function(event) {__eventHandler(event)});
	$("body").keypress(function(event) {__eventHandler(event)});
	$("body").change(function(event) {__eventHandler(event)});
	$(window).resize(function(event) {__eventHandler(event)});
	//$(window).scroll(function(event) {__eventHandler(event)});
});
function __onreadyHandler() {
	for (var i=0, il=__onready.length; i<il; i++) {
		var f = __onready[i].split('.');
		if(f.length == 2) {
			if (typeof window[f[0]][f[1]] == 'function') window[f[0]][f[1]]();
		}
		else {
			if (typeof window[f[0]] == 'function') window[f[0]][f[1]]();
		}
	}
}
function __eventHandler(event) {
	var keyCode;
	var uid = $(event.target).data('uid');
	var action = $(event.target).data('action') ? $(event.target).data('action') : 'default';
	if (event.type == "keypress" || event.type == "keydown" || event.type == "keyup") {
		keyCode = (event.keyCode) ? event.keyCode : event.which;
	}
	if (event.type == "keyup" && keyCode == 13) return;
	if (event.type == "keypress") action = "Keycode" + keyCode;
	if (event.type == "keydown") action = "Keycode" + keyCode;
	if (__onevent[event.type] != undefined) {
		var acta = action.split('_');
		var act = acta[0];
		var parm = acta[1];
		if (__onevent[event.type][act] != undefined) {
			var ea = __onevent[event.type][act].split('|');
			for(var i=0, il=ea.length; i<il; i++) {
				var f = ea[i].split('.');
				if(f.length == 2) window[f[0]][f[1]](uid, event, parm);
				else window[f[0]](uid, event, parm);
			}
			event.stopPropagation();
		}
	}
}
// load events in global event handling object
function __loadEvents(events) {
	for (i in events) {
		if (__onevent[i] == undefined) {
			__onevent[i] = events[i];
		}
		else {
			for (j in events[i]) {
				if (__onevent[i][j] == undefined) {
					__onevent[i][j] = events[i][j];
				}
				else {
					__onevent[i][j] += '|' + events[i][j];
				}
			}
		}
	}
}