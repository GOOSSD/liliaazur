﻿var mode="";
var focus="";
/*var slideid=-1;*/
var slidesid={};
var slidessh=[];

document.onkeydown = function(evt)
{
	evt = evt || window.event;
	switch (evt.keyCode)
	{
	case 37: /* key left */
		if(mode=="fullscreen"){fscrprv()}
		else{prev(focus)};
		break;
	case 122: /*F11 => full screen */
		break;
	default:
		if(mode=="fullscreen"){fscrnxt()}
		else{next(focus)};
		break;
	};
};

document.onclick = function(evt)
{
	var idn='';
	if(evt.target)
	{
		evt.stopPropagation();
		idn = evt.target.id;
	}
	else if(evt.srcElement)
	{
		window.event.cancelBubble=true;
		idn = evt.srcElement.id;
	};
	if(("_fullscrnxt_fullscrprv_fullscrclose_fsc_nxt_prv_clse_").indexOf('_'+idn+'_')==-1)
	{
		/*if(mode=='fullscreen'){fscrnxt()};*/
	};
};
window.onload = function()
{
	if(slidessh[0]!=undefined) {focus = slidessh[0]}; 
	var url =document.URL;
	if(url.indexOf('#')!=-1)
	{
		url=url.split('#');
		var slidenr = 1*url[1];
		var slides = window['slides_'+focus];
		if(slidenr > 0 && slidenr < slides.length+1)
		{
			slidesid['slides_'+focus]=slidenr-2;
		};
		if(url[2]!=undefined)
		{
			if(url[2]=='fullscreen')
			{
				slidesid['slides_'+focus]+=1;
				fullscr(focus);
				for(var i=0; i<slidessh.length; i++)
				{
					if(slidessh[i] != focus) next(slidessh[i], 'dontchangefocus');
				};
			}
			else
			{
				for(var i=0; i<slidessh.length; i++)
				{
					next(slidessh[i], 'dontchangefocus');
				};
			};		
		}
		else
		{
			for(var i=0; i<slidessh.length; i++)
			{
				next(slidessh[i], 'dontchangefocus');
			};
		};
	}
	else
	{
		for(var i=0; i<slidessh.length; i++)
		{
			next(slidessh[i], 'dontchangefocus');
		};
	};
};

function zxcFade(id,t,ms){
 var obj=document.getElementById(id),o=zxcFade['zxc'+id],f;
 if (obj&&!o){
  o=zxcFade['zxc'+id]={ };
 }
 if (o){
  clearTimeout(o.dly);
  f=o.now||0;
  t=typeof(t)=='number'&&t>=0&&t<=100?t:0;
  animate(o,obj,f,t,new Date(),(ms||1000)*Math.abs(f-t)/100+5);
 }
}
function animate(o,obj,f,t,srt,mS){
 var oop=this,ms=new Date().getTime()-srt,now=(t-f)/mS*ms+f;
 if (isFinite(now)){
  obj.style.filter='alpha(opacity='+now+')';
  obj.style.opacity=obj.style.MozOpacity=obj.style.WebkitOpacity=obj.style.KhtmlOpacity=now/100-.001;
  o.now=now;
 }
 if (ms<mS){
  o.dly=setTimeout(function(){ animate(o,obj,f,t,srt,mS); },10);
 }
}
function next(fcus, mode)
{
	if(mode!='dontchangefocus') {focus=fcus};
	var slides = window['slides_'+fcus];
	var slideid=slidesid['slides_'+fcus];
	if(slideid<slides.length-1)
	{
		slidesid['slides_'+fcus]+=1;
		slideid=slidesid['slides_'+fcus];
		if(document.getElementById("slide_"+fcus+ "_"+slideid)==undefined)
		{
			var wrp = document.createElement("div");
			wrp.setAttribute("id","slide_"+fcus+ "_"+slideid);
			wrp.setAttribute("class","wrap");  
			var slwr = document.getElementById("slidewrapper_"+fcus);
			slwr.appendChild(wrp);
			if(slides[slideid][2]=='html')
			{
				var txtslide = window['txtslide_html_'+fcus];
				var txt=txtslide.split('|slideid|').join(slideid).split('|content|').join(slides[slideid][0]).split('|text|').join(slides[slideid][1]);
				document.getElementById("slide_"+fcus+"_"+slideid).innerHTML = txt;
				zxcFade('slide_'+fcus+"_"+slideid,100,1000);updateslidenbrs(fcus);
			}
			else
			{
				var txtslide = window['txtslide_'+fcus];
				var txt=txtslide.split('|slideid|').join(slideid).split('|src|').join(slides[slideid][0]).split('|text|').join(slides[slideid][1]);
				document.getElementById("slide_"+fcus+"_"+slideid).innerHTML = txt;
				document.getElementById("img_"+fcus+"_"+slideid).onload=function(){zxcFade('slide_'+fcus+"_"+slideid,100,1000);updateslidenbrs(fcus);};
			};
		}
		else
		{
			zxcFade("slide_"+fcus+ "_"+slideid,100,1000);
		};
		if(slideid > 0)
		{
			zxcFade("slide_"+fcus+ "_"+(slideid-1),0,1000);
		};
	};
	updateslidenbrs(fcus);
	
};
function updateslidenbrs(fcus)
{
	var slides = window['slides_'+fcus];
	var slideid=slidesid['slides_'+fcus];
	if(document.getElementById("slideid_nbr_"+fcus)!=undefined)
	{
	    if (document.getElementById("slideid_nbr_" + fcus).innerText != undefined) {
	        document.getElementById("slideid_nbr_" + fcus).innerText = (slideid + 1) + ' / ' + (slides.length);
	    }
	    else {
	        document.getElementById("slideid_nbr_" + fcus).textContent = (slideid + 1) + ' / ' + (slides.length);
	    }
	};
};
function prev(fcus)
{
	focus=fcus;
	var slides = window['slides_'+fcus];
	var slideid=slidesid['slides_'+fcus];
	var txtslide = window['txtslide_'+fcus];
	
	if(slideid > 0)
	{
		slidesid['slides_'+fcus]-=1;
		slideid=slidesid['slides_'+fcus];
		if(document.getElementById("slide_"+fcus+ "_"+slideid)!=undefined)
		{
			zxcFade("slide_"+fcus+ "_"+slideid,100,1000);
		}
		else
		{
			var wrp = document.createElement("div");
			wrp.setAttribute("id","slide_"+fcus+ "_"+slideid);
			wrp.setAttribute("class","wrap");  
			var slwr = document.getElementById("slidewrapper_" + fcus);
			slwr.appendChild(wrp);
			if(slides[slideid][2]=='html')
			{
				var txtslide = window['txtslide_html_'+fcus];
				var txt=txtslide.split('|slideid|').join(slideid).split('|content|').join(slides[slideid][0]).split('|text|').join(slides[slideid][1]);
				document.getElementById("slide_"+fcus+"_"+slideid).innerHTML = txt;
				zxcFade('slide_'+fcus+"_"+slideid,100,1000);updateslidenbrs(fcus);
			}
			else
			{
				var txtslide = window['txtslide_'+fcus];
				var txt=txtslide.split('|slideid|').join(slideid).split('|src|').join(slides[slideid][0]).split('|text|').join(slides[slideid][1]);
				document.getElementById("slide_"+fcus+"_"+slideid).innerHTML = txt;
				document.getElementById("img_"+fcus+"_"+slideid).onload=function(){zxcFade('slide_'+fcus+"_"+slideid,100,1000);updateslidenbrs(fcus);};
			};
		};
		zxcFade('slide_'+fcus+"_"+(slideid+1),0,1000);
	};
	updateslidenbrs(fcus);
};
function fullscr(fcus)
{   
	var slideid=slidesid['slides_'+focus];
	if(slideid!=-1)
	{
		zxcFade('slide_'+focus+"_"+slideid,0,1000);
		slidesid['slides_'+focus]-=1;
	};
	focus=fcus;
	var slides = window['slides_'+fcus];
	slideid=slidesid['slides_'+fcus];
			
	mode="fullscreen";
	var overlay = document.createElement("div");
	overlay.setAttribute("id","overlay");
	overlay.setAttribute("class", "overlay");
	document.body.appendChild(overlay);   // create overlay and append to page
	
	var slidewrp = document.createElement("div");
	slidewrp.setAttribute("id","fscrslidewrapper");
	document.body.appendChild(slidewrp);   // create slidewrapper and append to page
	document.getElementById("fscrslidewrapper").position='absolute';
	document.getElementById("fscrslidewrapper").style.top=window.pageYOffset+'px'; 
	
	var btt = document.createElement("div");
	btt.setAttribute("id","bttns");
	btt.setAttribute("class","fscrbuttns");  
	var slwr = document.getElementById("fscrslidewrapper");
	slwr.appendChild(btt);
	var txtbutt = txtfscrbuttons.split('|slideid|').join(slideid+1).split('|nbrslides|').join(slides.length);
	document.getElementById("bttns").innerHTML = txtbutt;
	fscrnxt();
	
	
};
function fscrupdateslidenbrs()
{
	var slideid=slidesid['slides_'+focus];
	var slides = window['slides_'+focus];
	if(document.getElementById("fscrslideid_nbr")!=undefined)
	{
	    if (document.getElementById("fscrslideid_nbr").innerText != undefined) {
	        document.getElementById("fscrslideid_nbr").innerText = (slideid + 1) + ' / ' + (slides.length);
	    }
	    else {
	        document.getElementById("fscrslideid_nbr").textContent = (slideid + 1) + ' / ' + (slides.length);
	    }
	};
};
function fscrnxt()
{
	var slideid=slidesid['slides_'+focus];
	var slides = window['slides_'+focus];
	
	if(slideid<slides.length-1)
	{
		slidesid['slides_'+focus]+=1;
		slideid=slidesid['slides_'+focus];
		if(document.getElementById("fslide"+slideid)==undefined)
		{
			var wrp = document.createElement("div");
			wrp.setAttribute("id","fslide"+slideid);
			wrp.setAttribute("class","fscrwrap");  
			var slwr = document.getElementById("fscrslidewrapper");
			slwr.appendChild(wrp);
			
			if(slides[slideid][2]=='html')
			{
				var txt=txtfscrslide_html.split('|slideid|').join(slideid).split('|content|').join(slides[slideid][0]).split('|text|').join(slides[slideid][1]);
				document.getElementById("fslide"+slideid).innerHTML = txt;
				zxcFade('fslide'+slideid,100,1000);fscrupdateslidenbrs();
			}
			else
			{
				var txt=txtfscrslide.split('|slideid|').join(slideid).split('|src|').join(slides[slideid][0]).split('|text|').join(slides[slideid][1]);
				document.getElementById("fslide"+slideid).innerHTML = txt;
				document.getElementById("fimg"+slideid).onload=function(){zxcFade('fslide'+slideid,100,1000);fscrupdateslidenbrs();};
			};
		}
		else
		{
			zxcFade('fslide'+slideid,100,1000);
		};
		if(slideid > 0)
		{
			zxcFade('fslide'+(slideid-1),0,1000);
		};
	};
	fscrupdateslidenbrs();
};
function fscrprv()
{
	var slideid=slidesid['slides_'+focus];
	var slides = window['slides_'+focus];
	
	if(slideid > 0)
	{
		slidesid['slides_'+focus]-=1;
		slideid=slidesid['slides_'+focus];
		if(document.getElementById("fslide"+slideid)!=undefined)
		{
			zxcFade('fslide'+slideid,100,1000);
		}
		else
		{
			var wrp = document.createElement("div");
			wrp.setAttribute("id","fslide"+slideid);
			wrp.setAttribute("class","fscrwrap");  
			var slwr = document.getElementById("fscrslidewrapper");
			slwr.appendChild(wrp);
			if(slides[slideid][2]=='html')
			{
				var txt=txtfscrslide_html.split('|slideid|').join(slideid).split('|content|').join(slides[slideid][0]).split('|text|').join(slides[slideid][1]);
				document.getElementById("fslide"+slideid).innerHTML = txt;
				zxcFade('fslide'+slideid,100,1000);
				fscrupdateslidenbrs();
			}
			else
			{
				var txt=txtfscrslide.split('|slideid|').join(slideid).split('|src|').join(slides[slideid][0]).split('|text|').join(slides[slideid][1]);
				document.getElementById("fslide"+slideid).innerHTML = txt;
				document.getElementById("fimg"+slideid).onload=function()
				{
					zxcFade('fslide'+slideid,100,1000);
					fscrupdateslidenbrs();
				};
				
			};
		};
		zxcFade('fslide'+(slideid+1),0,1000);
	};
	fscrupdateslidenbrs();
};
function clse() 
{
	
	document.body.removeChild(document.getElementById("overlay"));
	document.body.removeChild(document.getElementById("fscrslidewrapper"));
	mode='';
	slidesid['slides_'+focus]-=1; next(focus);
};