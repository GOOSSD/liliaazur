﻿// initialize slick slideshow
__onready.push('slick.initTeaser');
__onready.push('slick.initSlider');

// define event handling
if (!slick) var slick = {};
slick.events = {
	"hashchanged":
	{
		"default": "slick.goTo"
	}
}

// load events in global event handling object
__loadEvents(slick.events);


__data['slick'] = '';
// functions
slick.initTeaser = function() {
	$('.teaser').on('init', 
		function(slick) {
            $('.teaser').fadeIn(3000);
        }).slick({
			initialSlide: 0,
			slidesToShow: 1,
			slidesToScroll: 1,
			lazyLoad: 'ondemand',
			autoplay: true,
			autoplaySpeed: 3000,
			fade: true
		})
};

slick.initSlider = function() {
	// check for slide info in url
	var url = window.location.href;
	var slide = url.split('#');
	var initSlide = 1;
	for (var i=1, il=slide.length; i<il; i++) {
		if (slide[i] && slide[i].substring(0,5) == 'slide') {
			var nr = slide[i].split('=');
			if (nr[1]) {
				initSlide = nr[1];
			}
			slide.splice(i,1);
			break;
		}
	}
	$('.slideshow').on('init', 
		function(slick) {
            $('.slider').fadeIn(3000);
        }).slick({
			initialSlide: initSlide-1,
			slidesToShow: 1,
			slidesToScroll: 1,
			lazyLoad: 'ondemand',
			autoplay: false,
			autoplaySpeed: 3000,
			fade: false
		}).on('afterChange',
		function(slick, currentSlide){
			if (window.history) {
				if (__data['slick'] == '') {
					history.pushState(null, "#slide="+(currentSlide.currentSlide+1), slide.join('#')+'#slide='+(currentSlide.currentSlide+1));
				}
				else {
					__data['slick'] = '';
				}
			}
		});
};

if("onhashchange" in window) {
    window.onhashchange = function(){
		var url = window.location.href;
		var slide = url.split('#');
		var initSlide = 1;
		for (var i=1, il=slide.length; i<il; i++) {
			if (slide[i] && slide[i].substring(0,5) == 'slide') {
				var nr = slide[i].split('=');
				if (nr[1]) {
					initSlide = nr[1];
				}
				slide.splice(i,1);
				break;
			}
		}
		console.log(initSlide);
		__data['slick'] = 'goTo';
		$('.slideshow').slick('slickGoTo', (initSlide>0)?initSlide-1:0);
	}
}
