// init some parameters
__data['project'] = 'efficy';
__data['fontSize'] = 16; // default global font-size
__data['breakpointPx'] = 1200; //  breakpoint layout in px

// initialize function
__onready.push('init.initFontSize');
__onready.push('init.unveil');

// define event handling
if (!init) var init = {};
init.events = {
	"click":
	{
		"init.increaseFontSize": "init.increaseFontSize"
		, "init.decreaseFontSize": "init.decreaseFontSize"
		, "init.setLanguage": "init.setLanguage"
	}
}
// load events in global event handling object
__loadEvents(init.events);

// helper functions
init._getFontSize = function() {
	var fontSizeParam = __data['project'] + '_fontSize';
	var fontSize;
	if (('localStorage' in window) && window.localStorage !== null) {
		fontSize = JSON.parse(localStorage.getItem(fontSizeParam));
	}
	return fontSize;
}
init._setFontSize = function(fontSize) {
	var fontSizeParam = __data['project'] + '_fontSize';
	__data['fontSize'] = fontSize;
	if (('localStorage' in window) && window.localStorage !== null) {
		localStorage.setItem(fontSizeParam, fontSize);
	}
	$('html').css('font-size', fontSize + 'px');
}

// functions
init.setLanguage = function(id, event, parm) {
	var languageParam = __data['project'] + '_language';
	if (('localStorage' in window) && window.localStorage !== null) {
		localStorage.setItem(languageParam, parm);
	}
}
init.initFontSize = function() {
	var fontSize = init._getFontSize();
	if (!fontSize) {
		fontSize = 16;
	}
	init._setFontSize(fontSize);
}
init.increaseFontSize = function() {
	var fontSize = init._getFontSize();
	if (!fontSize) {
		fontSize = 17;
	}
	fontSize += 1;
	init._setFontSize(fontSize);
}
init.decreaseFontSize = function() {
	var fontSize = init._getFontSize();
	if (!fontSize) {
		fontSize = 15;
	}
	fontSize -= 1;
	init._setFontSize(fontSize);
}
init.unveil = function() {
	$("img").unveil(100);
}