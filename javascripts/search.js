// initialize search and highlight function
__onready.push('hljs.initHighlightingOnLoad');
__onready.push('rangy.init');
__onready.push('search.highlightSearchWords');
__onready.push('search.searchWords');
__onready.push('search.searchRelatedContent');
//__onready.push('search.fixLayout');

// define event handling
if (!search) var search = {};
search.events = {
	"resize":
	{
		"default": "search.fixLayout|navigation.cleanupElevateZoom"
	}
	,"click":
	{
		"search.slide": "navigation.cleanupElevateZoom|search.searchSlide|search.hidePopups"
		, "search.search": "search.loadPreviousWords"
		, "search.searchMainSearch": "navigation.cleanupElevateZoom|search.advSearch"
		, "search.searchSlideInOutSearch": "search.advSearch|navigation.navigationSlide"
		, "search.searchWord": "navigation.cleanupElevateZoom|search.searchWord"
		, "search.removeWord": "search.removeWord"
		, "search.searchClear": "search.searchClear"
		, "default": "search.hidePopups"
	}
	,"dblclick": {"default": "search.getSelection|search.highlight"}
	,"change": {"search.searchtext": "search.updateSearchCancel"}
	,"keyup":
	{
		"search.search": "search.searchforMatch|search.highlight"
	}
	,"keypress": {"Keycode13": "navigation.cleanupElevateZoom|search.advSearch"}
}

// load events in global event handling object
__loadEvents(search.events);

// search result templates
search.result_content = '<h1>Search results for: <small>{{search-words}}</small></h1><ul id="contentContent"><ul class="homepage-list">{{search-result}}</ul></ul><br/><br/>';
search.result_slide_in_out = '<h1>Search results for: <small>{{search-words}}</small></h1><ul class="content content-content"><ul class="homepage-list" id="contentContent">{{search-result}}</ul></ul><br/><br/>';
search.related_content = '<h2><small>Related search results</small></h2><ul id="contentContent"><ul class="homepage-list">{{search-result}}</ul></ul><br/><br/>';
search.result_template = '<li class="full-width"><a href="{{href}}">{{name}}</a><div class="intro">{{intro}}</div></li>';
search.result_image_template = '<li><a href="{{href}}"><img class="{{layout}}" src="{{src}}"/></a><a href="{{href}}">{{name}}</a><div class="intro">{{intro}}</div></li>';

// functions
search.fixLayout = function() {
	if ($(window).width() > __data.breakpointPx) {
			$('.search-slide-in-out').animate({"left":"-100%"});
			$('.slide-in-out-search-toggle-button').find('i').removeClass('i-search-cancel').addClass('i-search');
			$('.slide-in-out-search-toggle-button').find('i').data('action', 'search.slide_out');
	}
}
search.searchSlide = function(uid, event, direction) {
	if (direction == 'out') {
		$('.search-slide-in-out').animate({"left":"0px"});
		$('.slide-in-out-search-toggle-button').find('i').removeClass('i-search').addClass('i-search-cancel');
		$('.slide-in-out-search-toggle-button').find('i').data('action', 'search.slide_in');
	}
	else {
		$('.search-slide-in-out').animate({"left":"-100%"});
		$('.slide-in-out-search-toggle-button').find('i').removeClass('.i-search-cancel').addClass('i-search');
		$('.slide-in-out-search-toggle-button').find('i').data('action', 'search.slide_out');
	}
}

search.showSearchResults = function() {
	$('#search_results_main').show(300);
	$('#search_results_slide_in_out').show(300);
}

search.hideSearchResults = function() {
	$('#search_results_main').hide(300);
	$('#search_results_slide_in_out').hide(300);
}

search.getSelection = function() {
	var txt = rangy.getSelection().toString();
	if (txt.length > 0) {
		$('#search_main_text').val(txt);
		$('#search_slide_in_out_text').val(txt);
		this.highlight();
	}
}
search.searchWords = function() {
	// check for search info in url
	var url = window.location.href;
	var search = url.split('#');
	for (var i=1, il=search.length; i<il; i++) {
		if (search[i] && search[i].substring(0,8) == 'dosearch') {
			var word = search[i].split('=');
			if (word[1]) {
				$('#search_main_text').val(decodeURIComponent(word[1]));
				$('#search_slide_in_out_text').val(decodeURIComponent(word[1]));
				this.updateSearchCancel();
				this.advSearch('', '', 'main');
			}
			break;	
		}
	} 
}
search.highlightSearchWords = function() {
	// check for search info in url
	var url = window.location.href;
	var search = url.split('#');
	for (var i=1, il=search.length; i<il; i++) {
		if (search[i] && search[i].substring(0,6) == 'search') {
			var word = search[i].split('=');
			if (word[1]) {
				$('#search_main_text').val(decodeURIComponent(word[1]));
				$('#search_slide_in_out_text').val(decodeURIComponent(word[1]));
				this.updateSearchCancel();
				this.highlight();
			}
			break;
		}
	}
}
search.searchRelatedContent = function() {
	// check for search info in url
	var url = window.location.href;
	var word, search = url.split('#');
	for (var i=1, il=search.length; i<il; i++) {
		if (search[i] && search[i].substring(0,6) == 'search') {
			word = search[i].split('=');
			if (word[1]) {
				$('#search_main_text').val(decodeURIComponent(word[1]));
				$('#search_slide_in_out_text').val(decodeURIComponent(word[1]));
				this.updateSearchCancel();
				//console.log(word[1]);
				this.search(word[1], 'showRelated');
			}
			break;
		}
	}
	if (!word) {
		// check for keywords of current page
		var keywords = [];
		for (var i in this.wordlist) {
			var kw = this.wordlist[i];
			if (kw[linkref]) {
				keywords.push({'w': i, 'cnt': kw[linkref]});
			}
		}
		keywords.sort(function (a, b) {return b.cnt - a.cnt;});
		if (keywords.length > 3) keywords.length = 3;
		var keys = [];
		for (var i in keywords) if (keywords[i].cnt > 5) keys.push(keywords[i].w);
		console.log(keys.join(' '));
		//this.search(keys.join(' '), 'showRelated');
	}
}
search.highlight = function() {
	if ($('ul.search-main-dropdown').hasClass('hide')) {
		var myHilitor = new Hilitor2("contentContent");
		myHilitor.setMatchType("open");
		myHilitor.apply($('#search_main_text').val().split('@').join(''));
	}
	else {
		var myHilitor = new Hilitor2("contentContent");
		myHilitor.remove();
	}
}
search.searchClear = function() {
	$('#search_main_text').val('');
	$('#search_slide_in_out_text').val('');
	this.hidePopups();
	this.updateSearchCancel();
	this.highlight();
	this.hideSearchResults();
}
search.hidePopups = function() {
	$('ul.search-main-dropdown').addClass('hide');
	$('ul.search-slide-in-out-dropdown').addClass('hide');
}
search.hideSearchCancel = function() {
	$('.search-main-clear > .i-clear').css('visibility','hidden');
	$('.search-slide-in-out-clear > .i-clear').css('visibility','hidden');
}
search.showSearchCancel = function(){
	$('.search-main-clear > .i-clear').css('visibility','visible');
	$('.search-slide-in-out-clear > .i-clear').css('visibility','visible');
}
search.updateSearchCancel = function(text) {
	if (!text) text = $('#search_main_text').val().trim();
	if (text.length > 0) {
		this.showSearchCancel();
	}
	else {
		this.hideSearchCancel();
	}
}
search.searchforMatch = function(uid, event, parm) {
	var sfmatch;
	if (!this.wordlist) return;
	if (parm == 'main') {
		sfmatch = $('#search_main_text').val().trim().toLowerCase();
		$('#search_slide_in_out_text').val(sfmatch);
	}
	else {
		sfmatch = $('#search_slide_in_out_text').val().trim().toLowerCase();
		$('#search_main_text').val(sfmatch);
	}
	this.updateSearchCancel(sfmatch);
	if (sfmatch.length < 3) {
		this.loadPreviousWords(uid, event, parm);
	}
	else {
		var list = [];
		for (w in this.wordlist) {
			if (w.indexOf(sfmatch) != -1) {
				list.push([w, w.split(sfmatch).join('<b data-action="search.searchWord" data-word="' + w + '">' + sfmatch + '</b>')]);
			}
		}
		if (list.length > 0) {
			var txt = [];
			for (var i=0; i<list.length; i++) {
					txt.push('<li data-action="search.searchWord" data-word="' + list[i][0] + '">' + list[i][1] + '</li>');
			}
			if (parm == 'main') {
				$('ul.search-main-dropdown').html(txt.join(''));
				$('ul.search-main-dropdown').removeClass('hide');
			}
			else {
				$('ul.search-slide-in-out-dropdown').html(txt.join(''));
				$('ul.search-slide-in-out-dropdown').removeClass('hide');
			}
		}
		else {
			if (parm == 'main') {
				$('ul.search-main-dropdown').html('');
				$('ul.search-main-dropdown').addClass('hide');
			}
			else {
				$('ul.search-slide-in-out-dropdown').html('');
				$('ul.search-slide-in-out-dropdown').addClass('hide');
			}
		}
	}
}
search.fixSrc = function(src) {
	var curUrl = document.location.pathname;
	var ca = curUrl.split('/');
	var ta = src.split('/');
	var ua = [];
	var j = 0;
	for (var i=0; i<ca.length; i++) {
		if (ca[i] == ta[0]) j+=1;
		else if (j > 0) break;
	}
	i+=1;
	for (; i<ca.length; i++) {
		ua.push('..');
	}
	for (; j<ta.length; j++) {
		ua.push(ta[j]);
	}
	return ua.join('/');
}
search.fixUrl = function(url, word) {
	word = encodeURIComponent(word);
	/*
	current:
	file:///C:/wwwebify/projects/EfficyDoc/Customization%20Efficy%202014/
	1_en/1_Technology%20overview/1_Overview%20of%20the%20Scripting%20Framework/3_The%20Database%20object/
	target:
	1_en/1_Technology%20overview/5_Examples/4_Scripts%20used%20by%20DataSync%20Tool/3_Remote%20DataSync%20Scripts/3_Using%20a%20Dataset/1_Using%20a%20Dataset%20with%20DelphiScript.html
	*/
	var curUrl = document.location.pathname;
	var ca = curUrl.split('/');
	var ta = url.split('/');
	var ua = [];
	var j = 0;
	for (var i=0; i<ca.length; i++) {
		if (ca[i] == ta[0]) j+=1;
		else if (j > 0) break;
	}
	i+=1;
	for (; i<ca.length; i++) {
		ua.push('..');
	}
	for (; j<ta.length; j++) {
		ua.push(ta[j]);
	}
	var url = ua.join('/');
	
	// check if not the same then current page
	var a = document.createElement('a');
	a.href = url; // parse url
	
	var curUrl = location.href.split('#')[0];
	var toUrl = a.href.split('#')[0]
	//alert(curUrl + "%%%%%%" + toUrl);
	if (curUrl == toUrl) {
		location.href = url + '#search=' + word;
		return 'javascript:location.reload();';
	}
	return ua.join('/') + '#search=' + word;
}
search.advSearch = function(uid, event, parm) {
	
	//var word = $(event.target).data('word');
	var word = $('#search_main_text').val().trim().toLowerCase();
	if (word.length == 0) {
		this.loadPreviousWords(uid, event, parm);
	}
	else {
		this.addWord(word);
		$('ul.search-main-dropdown').html('');
		$('ul.search-main-dropdown').addClass('hide');
		$('ul.search-slide-in-out-dropdown').html('');
		$('ul.search-slide-in-out-dropdown').addClass('hide');
		this.search(word, '');
	}
}
search.search = function(word, related) {	
	if (!this.wordlist || !this.linkref) return;
	if (word.length > 0) {
		var wlist = word.split(/\s+/g);
		var rlist = [];
		for (var j=0; j<wlist.length; j++) {
			for (w in this.wordlist) {
				if (w.indexOf(wlist[j]) != -1) {
					rlist.push([j, this.wordlist[w]]);
				}
			}
		}
		// consolidate for each word
		var clist = {};
		for (var j=0; j<rlist.length; j++) {
			var w = 'w' + rlist[j][0];
			if (clist[w] == undefined) {
				clist[w] = {};
			}
			for (var rl in rlist[j][1]) {
				if (clist[w][rl] == undefined) {
					clist[w][rl] = rlist[j][1][rl];
				}
				else {
					if (clist[w][rl] < rlist[j][1][rl]) {
						clist[w][rl] = rlist[j][1][rl];
					}
				}
			}
		}
		// make a global score for each link in consolidated list
		var gslist = {};
		for (var j in clist) {
			for (var k in clist[j]) {
				if (gslist[k] == undefined) {
					gslist[k] = 1000 + clist[j][k];
				}
				else {
					gslist[k] += 1000 + clist[j][k];
				}
			}
		}
		// sort and send to the webpage
		var links = [];
		for (link in gslist) {
			links.push({ppname: this.linkref[link].ppname, pname: this.linkref[link].pname, //pmore: this.linkref[link].pmore,
						href: this.fixUrl(this.linkref[link].href, word),
						src: (this.linkref[link]['first-image-src']) ? this.fixSrc(this.linkref[link]['first-image-src']) : '',
						layout: (this.linkref[link]['first-image-src']) ? this.linkref[link]['layout'] : '',
						name: this.linkref[link].name,
						intro: (this.linkref[link]['intro']) ? this.linkref[link]['intro'][0] : '',
						count: gslist[link]});
		}
		if (links.length > 0) {
			links.sort(function(a, b){return b.count-a.count});
			var txt = [];
			var txtRelated = [];
			for (var j = 0; j<links.length; j++) {
				var ppname = (links[j].ppname != '') ? links[j].ppname + ' / ' : '';
				var pname = (links[j].pname != '') ? links[j].pname + ' / ' : '';
				//var pmore = (links[j].pmore) ? ' ... / ' : '';
				var templ = (links[j].src) ? this.result_image_template : this.result_template;
				if (links[j].href) templ = templ.split('{{href}}').join(links[j].href);
				if (links[j].src) {
					templ = templ.split('{{src}}').join(links[j].src);
					templ = templ.split('{{layout}}').join(links[j].layout);
				}
				if (links[j].name) templ = templ.split('{{name}}').join(ppname + pname + links[j].name);
				if (links[j].intro) templ = templ.split('{{intro}}').join(links[j].intro);
				else templ = templ.split('{{intro}}').join('');
				if (links[j].tags) templ = templ.split('{{tags}}').join(links[j].tags);
				else templ = templ.split('{{tags}}').join('');
				txt.push(templ);
				
				// check if not the same then current page
				var a = document.createElement('a');
				a.href = links[j].href; // parse url
				var curUrl = window.location.href.split('#')[0];
				if (a.href != 'javascript:location.reload();') { 
					txtRelated.push(templ);
				} 
				
				//txt.push('<li>' + ppname + pname + /*pmore +*/ '<a href="' + links[j].href + '"><img src="' + links[j].src + '" />' + links[j].name + '</a></li>');
			}
			if (related != 'showRelated') {
				$('#content').addClass("content");
				$('#content').addClass("content-content");
				$('#content').addClass("content-full-width");
				$('#content').html(this.result_content.split('{{search-words}}').join(word).split('{{search-result}}').join(txt.join('')));
				$('#search_results_slide_in_out').html(this.result_slide_in_out.split('{{search-words}}').join(word).split('{{search-result}}').join(txt.join('')));
			}
			else {
				if (txtRelated.length) $('#contentRelated').html(this.related_content.split('{{search-result}}').join(txtRelated.join('')));
			}			
			this.highlight();
		}
		else {
			$('#content').addClass("content");
			$('#content').addClass("content-full-width");
			$('#content').html(this.result_content.split('{{search-words}}').join(word).split('{{search-result}}').join(''));
			$('#search_results_slide_in_out').html(this.result_slide_in_out.split('{{search-words}}').join(word).split('{{search-result}}').join(''));
		}
	}
	this.showSearchResults();
}
search.searchWord = function(i, event) {
	var word = $(event.target).data('word');
	$('ul.search-main-dropdown').addClass('hide');
	$('ul.search-slide-in-out-dropdown').addClass('hide');
	// save in local storage at the first place
	this.addWord(word);
	// show selection in search textbox
	$('#search_main_text').val(word);
	$('#search_slide_in_out_text').val(word);
	this.updateSearchCancel();
	this.search(word);
}
search.removeWord = function(id, event, parm) {
	if (('localStorage' in window) && window.localStorage !== null) {
		var w = $(event.target).data('word');
		var newList = [];
		var prevWords = JSON.parse(localStorage[this.project]);
		for (var i=0; i<prevWords.length; i++) {
			if (prevWords[i] != w) {
				newList.push(prevWords[i]);
			}
		}
		localStorage[this.project] = JSON.stringify(newList);
		this.loadPreviousWords(id, event, parm);
	}
}
search.addWord = function(w) {
	if (('localStorage' in window) && window.localStorage !== null) {
		var prevWords = JSON.parse(localStorage[this.project]);
		var i = prevWords.indexOf(w);
		if (i != -1) prevWords.splice(i, 1);
		prevWords.unshift(w);
		if (prevWords.length > 20) {
			prevWords.length = 20;
		}
		localStorage[this.project] = JSON.stringify(prevWords);
	}
}
search.loadPreviousWords = function(uid, event, parm) {
	var sfmatch;
	if (parm == 'main') {
		sfmatch = $('#search_main_text').val();
		$('#search_slide_in_out_text').val(sfmatch);
	}
	else {
		sfmatch = $('#search_slide_in_out_text').val();
		$('#search_main_text').val(sfmatch);
	}
	if (sfmatch.length > 2) {
		this.searchforMatch('', '', parm);
	}
	else {
		if (('localStorage' in window) && window.localStorage !== null) {
			if (localStorage[this.project] != undefined) {
				var prevWords = JSON.parse(localStorage[this.project]);
				if (prevWords.length > 0) {
					var txt = [];
					for (var i=0; i<prevWords.length; i++) {
						txt.push('<li data-action="search.searchWord" data-word="' + prevWords[i] + '">' + prevWords[i] + '<i class="i-clear" data-action="search.removeWord' + '_' + parm + '" data-word="' + prevWords[i] + '"></i></li>');
					}
					if (parm == 'main') {
						$('ul.search-main-dropdown').html(txt.join(''));
						$('ul.search-main-dropdown').removeClass('hide');
					}
					else {
						$('ul.search-slide-in-out-dropdown').html(txt.join(''));
						$('ul.search-slide-in-out-dropdown').removeClass('hide');
					}
				}
				else {
					if (parm == 'main') {
						$('ul.search-main-dropdown').html('');
						$('ul.search-main-dropdown').addClass('hide');
					}
					else {
						$('ul.search-slide-in-out-dropdown').html('');
						$('ul.search-slide-in-out-dropdown').addClass('hide');
					}
				}
			}
			else {
				localStorage[this.project] = JSON.stringify([]);
			}
		}
	}
}

// search terms highlighter

// Original JavaScript code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header.

function Hilitor2(id, tag)
{

  var targetNode = document.getElementById(id) || document.body;
  var hiliteTag = tag || "EM";
  var skipTags = new RegExp("^(?:" + hiliteTag + "|SCRIPT|FORM)$");
  var colors = ["#ff6", "#a0ffff", "#9f9", "#f99", "#f6f"];
  var wordColor = [];
  var colorIdx = 0;
  var matchRegex = "";
  var openLeft = false;
  var openRight = false;

  this.setMatchType = function(type)
  {
    switch(type)
    {
      case "left":
        this.openLeft = false;
        this.openRight = true;
        break;
      case "right":
        this.openLeft = true;
        this.openRight = false;
        break;
      case "open":
        this.openLeft = this.openRight = true;
        break;
      default:
        this.openLeft = this.openRight = false;
    }
  };

  function addAccents(input)
  {
    retval = input;
    retval = retval.replace(/([ao])e/ig, "$1");
    retval = retval.replace(/\\u00E[024]/ig, "a");
    retval = retval.replace(/\\u00E[89AB]/ig, "e");
    retval = retval.replace(/\\u00E[EF]/ig, "i");
    retval = retval.replace(/\\u00F[46]/ig, "o");
    retval = retval.replace(/\\u00F[9BC]/ig, "u");
    retval = retval.replace(/\\u00FF/ig, "y");
    retval = retval.replace(/\\u00DF/ig, "s");
    retval = retval.replace(/a/ig, "([aàâä]|ae)");
    retval = retval.replace(/e/ig, "[eèéêë]");
    retval = retval.replace(/i/ig, "[iîï]");
    retval = retval.replace(/o/ig, "([oôö]|oe)");
    retval = retval.replace(/u/ig, "[uùûü]");
    retval = retval.replace(/y/ig, "[yÿ]");
    retval = retval.replace(/s/ig, "(ss|[sß])");
    return retval;
  }

  this.setRegex = function(input)
  {
    input = input.replace(/\\([^u]|$)/g, "$1");
    input = input.replace(/[^\w\\\s']+/g, "").replace(/\s+/g, "|");
    input = addAccents(input);
    var re = "(" + input + ")";
    if(!this.openLeft) re = "(?:^|[\\b\\s])" + re;
    if(!this.openRight) re = re + "(?:[\\b\\s]|$)";
    matchRegex = new RegExp(re, "i");
  };

  this.getRegex = function()
  {
    var retval = matchRegex.toString();
    retval = retval.replace(/(^\/|\(\?:[^\)]+\)|\/i$)/g, "");
    return retval;
  };

  // recursively apply word highlighting
  this.hiliteWords = function(node)
  {
    if(node === undefined || !node) return;
    if(!matchRegex) return;
    if(skipTags.test(node.nodeName)) return;

    if(node.hasChildNodes()) {
      for(var i=0; i < node.childNodes.length; i++)
        this.hiliteWords(node.childNodes[i]);
    }
    if(node.nodeType == 3) { // NODE_TEXT
      if((nv = node.nodeValue) && (regs = matchRegex.exec(nv))) {
        if(!wordColor[regs[1].toLowerCase()]) {
          wordColor[regs[1].toLowerCase()] = colors[colorIdx++ % colors.length];
        }

        var match = document.createElement(hiliteTag);
        match.appendChild(document.createTextNode(regs[1]));
        match.style.backgroundColor = wordColor[regs[1].toLowerCase()];
        match.style.fontStyle = "inherit";
        match.style.color = "#000";

        var after;
        if(regs[0].match(/^\s/)) { // in case of leading whitespace
          after = node.splitText(regs.index + 1);
        } else {
          after = node.splitText(regs.index);
        }
        after.nodeValue = after.nodeValue.substring(regs[1].length);
        node.parentNode.insertBefore(match, after);
      }
    };
  };

  // remove highlighting
  this.remove = function()
  {
    var arr = document.getElementsByTagName(hiliteTag);
    while(arr.length && (el = arr[0])) {
      var parent = el.parentNode;
      parent.replaceChild(el.firstChild, el);
      parent.normalize();
    }
  };

  // start highlighting at target node
  this.apply = function(input)
  {
    this.remove();
    if(input === undefined || !(input = input.replace(/(^\s+|\s+$)/g, ""))) return;
    input = convertCharStr2jEsc(input);
    this.setRegex(input);
    this.hiliteWords(targetNode);
  };

  // added by Yanosh Kunsh to include utf-8 string comparison
  function dec2hex4(textString)
  {
    var hexequiv = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
    return hexequiv[(textString >> 12) & 0xF] + hexequiv[(textString >> 8) & 0xF] + hexequiv[(textString >> 4) & 0xF] + hexequiv[textString & 0xF];
  }

  function convertCharStr2jEsc(str, cstyle)
  {
    // Converts a string of characters to JavaScript escapes
    // str: sequence of Unicode characters
    var highsurrogate = 0;
    var suppCP;
    var pad;
    var n = 0;
    var outputString = '';
    for(var i=0; i < str.length; i++) {
      var cc = str.charCodeAt(i);
      if(cc < 0 || cc > 0xFFFF) {
        outputString += '!Error in convertCharStr2UTF16: unexpected charCodeAt result, cc=' + cc + '!';
      }
      if(highsurrogate != 0) { // this is a supp char, and cc contains the low surrogate
        if(0xDC00 <= cc && cc <= 0xDFFF) {
          suppCP = 0x10000 + ((highsurrogate - 0xD800) << 10) + (cc - 0xDC00);
          if(cstyle) {
            pad = suppCP.toString(16);
            while(pad.length < 8) {
              pad = '0' + pad;
            }
            outputString += '\\U' + pad;
          } else {
            suppCP -= 0x10000;
            outputString += '\\u' + dec2hex4(0xD800 | (suppCP >> 10)) + '\\u' + dec2hex4(0xDC00 | (suppCP & 0x3FF));
          }
          highsurrogate = 0;
          continue;
        } else {
          outputString += 'Error in convertCharStr2UTF16: low surrogate expected, cc=' + cc + '!';
          highsurrogate = 0;
        }
      }
      if(0xD800 <= cc && cc <= 0xDBFF) { // start of supplementary character
        highsurrogate = cc;
      } else { // this is a BMP character
        switch(cc)
        {
          case 0:
            outputString += '\\0';
            break;
          case 8:
            outputString += '\\b';
            break;
          case 9:
            outputString += '\\t';
            break;
          case 10:
            outputString += '\\n';
            break;
          case 13:
            outputString += '\\r';
            break;
          case 11:
            outputString += '\\v';
            break;
          case 12:
            outputString += '\\f';
            break;
          case 34:
            outputString += '\\\"';
            break;
          case 39:
            outputString += '\\\'';
            break;
          case 92:
            outputString += '\\\\';
            break;
          default:
            if(cc > 0x1f && cc < 0x7F) {
              outputString += String.fromCharCode(cc);
            } else {
              pad = cc.toString(16).toUpperCase();
              while(pad.length < 4) {
                pad = '0' + pad;
              }
              outputString += '\\u' + pad;
            }
        }
      }
    }
    return outputString;
  }
}